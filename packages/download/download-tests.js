// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by download.js.
import { name as packageName } from "meteor/nchokoev:download";

// Write your tests here!
// Here is an example.
Tinytest.add('download - example', function (test) {
  test.equal(packageName, "download");
});
