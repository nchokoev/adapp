// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See download-tests.js for an example of importing.
export const name = 'download';

var download = require('download-file');
//var data = require('fs');

function downloadImage(url, dir, title, cb) {
    // extracting the name of the file from the downloaded file.
    var fileName = "";
    if(!title || title === ""){
        fileName = url.substr((url.lastIndexOf('/') + 1));
    } else {
        fileName = title + "." + url.substr((url.lastIndexOf('.') + 1));
    }
    
    var options = {
        directory: dir,
        filename: fileName
    }
    download(url, options, function(err){
        if (err) {
            console.log(err);
            throw err
        }
        if(cb){
            console.log(fileName);
            cb(fileName);
        }        
    });
    //Returning the downloaded file name.
    return fileName;
}

Meteor.startup(function () {
    Meteor.methods({
        downloadImage: function(url, dir, title, cb){
            return downloadImage(url, dir, title, cb);
        }
    });
});

