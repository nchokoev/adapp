/**
 * Created by avolkov on 29.11.16.
 */

const HOME_PAGE = "https://www.donedeal.ie";
const PLACEAD_PAGE = "https://www.donedeal.ie/adwrite/publish";


var utils = require('utils');
var settingsFile = casper.cli.get('settings');

if(!settingsFile){
    console.log("No settings file.");
    casper.exit();
}

var fs = require('fs');
var set = fs.read(settingsFile);
var s = JSON.parse(set);

// var casper  = require('casper').create({
//     verbose: true,
//     pageSettings: {
//         webSecurityEnabled: false
//     }
// });

casper.options.viewportSize = {width: 1600, height: 950};
casper.options.waitTimeout = 100000;

// casper.selectOptionByValue = function(selector, valueToMatch){
//     this.evaluate(function(selector, valueToMatch){
//         var select = document.querySelector(selector),
//             found = false;
//         Array.prototype.forEach.call(select.children, function(opt, i){
//             if (!found && opt.value.indexOf(valueToMatch) !== -1) {
//                 select.selectedIndex = i;
//                 found = true;
//             }
//         });
//         // dispatch change event in case there is some kind of validation
//         var evt = document.createEvent("UIEvents"); // or "HTMLEvents"
//         evt.initUIEvent("change", true, true);
//         select.dispatchEvent(evt);
//     }, selector, valueToMatch);
// };

// casper.selectOptionByValue('#summaryLevel', '050');

casper.test.begin('Testing adding ads', 0, function suite(test) {
    test.comment('Loading ' + HOME_PAGE + '...');
    casper.start(HOME_PAGE, function () {
        test.comment('Main page loaded');
    });

    // open login form
    casper.then(function() {
        //test.assert(false);
        this.click(".header-actions-login_signup .icon-login");
        this.sendKeys('input[name=email]', s.login);
        this.sendKeys('input[name=password]', s.password);
        this.clickLabel('Log in', 'button');
    });
    casper.wait(1000);

    // filling login form
    casper.then(function() {
        var res;
        res = casper.evaluate(function () {
            return document.querySelector(".header-menu-trigger > .header-menu-name.ng-binding").innerHTML;
        });
        test.comment('Successfully logged in as ' + res);
        test.assertEquals(s.username, res);
    });

    // navigate to /adwrite/publish page
    casper.thenOpen(PLACEAD_PAGE, function () {
        test.comment('Place ad page loaded');
        test.assertUrlMatch(this.getCurrentUrl(), PLACEAD_PAGE);
    });

    // upload all photos and videos from settings
    casper.then(function() {
        for (var i = 0; i < s.paths.photos.length; i++) {
            casper.page.uploadFile("#upload-btn", s.paths.photos[i]);
            test.comment('Uploaded from path ' + s.paths.photos[i]);
        }
        for (var i = 0; i < s.paths.videos.length; i++) {
            casper.page.uploadFile("#upload-btn", s.paths.videos[i]);
            test.comment('Uploaded from path ' + s.paths.videos[i]);
        }
   });

    // check photos are loaded
    casper.then(function() {
        for (var i = 1; i <= s.paths.photos.length; i++) {
            casper.waitForSelector("#thumb-" + i, function _then() {});
        }
    });

    // filling other fields
    casper.then(function() {
        this.sendKeys('#ad-title', s.ad_sets.title);
        test.comment('Filling ad title: ' + s.ad_sets.title);

        this.sendKeys('#price', s.ad_sets.price);
        test.comment('Filling ad price: ' + s.ad_sets.price);

        this.sendKeys('#ad-desc', s.ad_sets.description);
    });
    casper.wait(500);

    // selecting section
    casper.then(function () {
        this.evaluate(function () {
            $(".dropdown.top-section-dd>.cust-select").click();
        });
        
        var value = s.ad_section.section_index;//"babyandkids";
        this.evaluate(function(value) {
            var sel = document.querySelector('#section');
            sel.selectedIndex = value;
            var evt = document.createEvent("UIEvents");
            evt.initUIEvent("change", true, true);
            sel.dispatchEvent(evt);
        }, value);
        test.comment('Section selected: ' + value);
    });

    // selecting subsection
    casper.then(function () {
        casper.waitForSelector(".dropdown.sub-section-dd>.cust-select", function _then() {
            this.evaluate(function () {
                $(".dropdown.sub-section-dd>.cust-select").click();
            });
            var value = s.ad_section.sub_section_index;//"feeding";
            this.evaluate(function(value) {
                var sel = document.querySelector('#subsection');
                sel.selectedIndex = value;
                var evt = document.createEvent("UIEvents");
                evt.initUIEvent("change", true, true);
                sel.dispatchEvent(evt);
            }, value);
            test.comment('Subsection selected: ' + value);
        }, function _onTimeout(){
            this.echo(".dropdown.sub-section-dd>.cust-select not found", "WARNING");
        });        
    });

    casper.then(function () {
        // #upsell-package_Free
        // #upsell-package_Regular
        casper.waitForSelector("#upsell-package_Free", function _then() {
            this.evaluate(function () {
                $("#upsell-package_Free").click();
            });
        }, function _onTimeout(){
            this.echo("#upsell-package_Free not found", "WARNING");
        });        
    });

    //submitting an ad
     casper.then(function () {
        test.comment('Submitting ad');
        casper.waitForSelector("button[id='ad-submit']:enabled",
            function success() {
                casper.wait(500);
                this.capture(s.dirPath +'/BeforeSubmit.png');
                this.evaluate(function() {
                    try {
                        document.getElementById("ad-submit").click();            
                    } catch(exception) {
                        if (exception.name == 'NetworkError') {
                            test.comment('There was a network error.');
                        }
                    }
                });
            },
            function fail() {
                test.comment('Submit button disabled too long.');
            });
     });

    casper.wait(1000);
    casper.then(function(){
        this.capture(s.dirPath + '/AfterSubmit.png');
    });

    // check that ad placed on payment page
    casper.then(function () {
        this.evaluate(function() {
            var item_in_card_exists = $('body > main > div > nav > div > div > span:nth-child(3) > span').text();
            test.assertTruthy(item_in_card_exists);
            if(item_in_card_exists == "Ad placed"){
                test.comment('All OK.');
            } else {
                casper.test.assert(true, "Ad not placed.");
            }
        });
    });

    casper.run(function () {
        test.done();
    });
});
