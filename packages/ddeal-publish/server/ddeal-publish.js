// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See ddeal-publish-tests.js for an example of importing.
export const name = 'ddeal-publish';
Fiber = Npm.require("fibers");

var fs   = require('fs'),
    util = require('util'),
    path = require('path'),
    exec = require('child_process').exec,
    spawn = require('child_process').spawn;

function runCasper(scriptPath, inputPath, cb){
    child = spawn('casperjs', ['test', "--settings="+inputPath, "--fail-fast", scriptPath])
    child.stdout.on('data', function(msg){
        process.stdout.write(msg);
    });
    child.stderr.on('data', function(msg){
        process.stderr.write(msg);
    });
    child.on('exit', function(code){
        if (code > 0){
            console.log("Casperjs exited with code "+code);
            if(cb) cb(code,null);
        } else {
            if(cb) cb(null,null);
        }
    });
}

Meteor.methods({
    "ddeal_pre_check":function(aid, s, dirPath, password){
        var doc= Ddeal.findOne({aid:aid});
        if(!doc){
            throw new Meteor.Error( 500, 'Data not available');
        }
        if(!doc.login){
            throw new Meteor.Error( 500, 'Login not set');
        }
        if(!doc.username){
            throw new Meteor.Error( 500, 'Username not set');
        }
        if(!doc.ad_sets || !doc.ad_sets.price){
            throw new Meteor.Error( 500, 'Price not set');
        }
        if(!doc.ad_section || !doc.ad_section.section_index || !doc.ad_section.sub_section_index){
            throw new Meteor.Error( 500, 'Section not set');
        }
        return;
    },
    "ddeal_run":function(aid, s, dirPath, password){
        var doc= Ddeal.findOne({aid:aid});
        doc["password"] = password;
        doc["paths"]= s["paths"];
        doc.ad_sets["title"]= s.ad_sets.title;
        doc.ad_sets["description"]= s.ad_sets.description;
        doc["dirPath"]=dirPath;
        var inputPath = path.join(dirPath, 'config.json');
        var scriptPath = path.join(dirPath, 'SubmitAdTest.js');
        var scriptLocalPath = "/Users/admin/work/Projects/adapp/adapp_meteor/packages/ddeal-publish/SubmitAdTest.js";
        var data = fs.readFileSync(scriptLocalPath);
        fs.writeFileSync(scriptPath, data);
        fs.writeFileSync(inputPath, JSON.stringify(doc));
        var runCasperWrapped = Meteor.wrapAsync(runCasper);
        return runCasperWrapped(scriptPath, inputPath);
    }
});