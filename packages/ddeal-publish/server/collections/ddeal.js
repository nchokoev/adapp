Ddeal.allow({
	insert: function (userId, doc) {
		return Ddeal.userCanInsert(userId, doc);
	},

	update: function (userId, doc, fields, modifier) {
		return Ddeal.userCanUpdate(userId, doc);
	},

	remove: function (userId, doc) {
		return Ddeal.userCanRemove(userId, doc);
	}
});
