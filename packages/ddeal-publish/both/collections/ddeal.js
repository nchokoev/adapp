this.Ddeal = new Mongo.Collection("ddeal");

this.Ddeal.userCanInsert = function(userId, doc) {
	return true;
};

this.Ddeal.userCanUpdate = function(userId, doc) {
	return true;
};

this.Ddeal.userCanRemove = function(userId, doc) {
	return true;
};