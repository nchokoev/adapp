// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See ddeal-publish-tests.js for an example of importing.
export const name = 'ddeal-publish';
Meteor.subscribe("ddeal_list");

Template.ddealTemplate.rendered = function() {
    Session.set("ddeal_publish", false);
};

function saveData(e,t){
    var doc = Ddeal.findOne({aid:t.data.params._id});
    if(doc){
        Ddeal.update({_id:doc._id},{
            $set:{
                login: $('input[name="login"]').val(),
                username: $('input[name="username"]').val(),
                ad_sets: {
                    price: $('input[name="price"]').val()
                },
                ad_section: {
                    section_index: $('input[name="category"]').val(),
                    sub_section_index: $('input[name="subcategory"]').val()
                }
            }
        });             
    } else {
        Ddeal.insert({
            aid:t.data.params._id,
            login: $('input[name="login"]').val(),
            username: $('input[name="username"]').val(),
            ad_sets: {
                price: $('input[name="price"]').val()
            },
            ad_section: {
                section_index: $('input[name="category"]').val(),
                sub_section_index: $('input[name="subcategory"]').val()
            }
        });
    }
}
Template.ddealTemplate.events({
	"click button.save":function(e,t){
        saveData(e, t);
    },
	"click button.savenpublish":function(e,t){
        saveData(e, t);
    },
    "click input.publish":function(e,t){
        Session.set("ddeal_publish", e.target.checked);
    }
});

Template.ddealTemplate.helpers({
    "id":function(){
        var t=Ddeal.findOne({aid:this.params._id});
        if(t && t._id){
            return t._id;
        }
    },
    "storepassword":function(){
        return false;
    },
	"username":function(){
        var t=Ddeal.findOne({aid:this.params._id});
        if(t && t.username){
            return t.username;
        }
    },
	"login":function(){
        var t=Ddeal.findOne({aid:this.params._id});
        if(t && t.login){
            return t.login;
        }
    },
    "ddeal_password":function(){
        var t=Ddeal.findOne({aid:this.params._id});
        if(t && t.password){
            return t.password;
        }
    },
	"price":function(){
        var t=Ddeal.findOne({aid:this.params._id});
        if(t && t.ad_sets && t.ad_sets.price){
            return t.ad_sets.price;
        }
    },
	"category":function(){
        var t=Ddeal.findOne({aid:this.params._id});
        if(t && t.ad_section && t.ad_section.section_index){
            return t.ad_section.section_index;
        }
    },
	"subcategory":function(){
        var t=Ddeal.findOne({aid:this.params._id});
        if(t && t.ad_section && t.ad_section.sub_section_index){
            return t.ad_section.sub_section_index;
        }
    }
});