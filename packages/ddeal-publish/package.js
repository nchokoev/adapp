Package.describe({
  name: 'nchokoev:ddeal-publish',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.0.1');
  api.use('templating','client');
  api.use('maxkferg:temp');
  api.use('ecmascript');
  api.use('mongo', ['client', 'server']);
  api.addFiles('both/collections/ddeal.js');
  api.addFiles('server/collections/ddeal.js','server');
  api.addFiles('server/publish/ddeal.js','server');
  api.mainModule('server/ddeal-publish.js','server');
  api.addFiles('client/ddeal-publish.html','client');
  api.addFiles('client/ddeal-publish.js','client');
});

Package.onTest(function(api) {
  api.use('maxkferg:temp');
  api.use('ecmascript');
  api.use('tinytest');
  api.use('mongo', ['client', 'server']);
  api.use('nchokoev:ddeal-publish');
  api.mainModule('ddeal-publish-tests.js');
});
