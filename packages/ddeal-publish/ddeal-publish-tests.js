// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by ddeal-publish.js.
import { name as packageName } from "meteor/nchokoev:ddeal-publish";

// Write your tests here!
// Here is an example.
Tinytest.add('ddeal-publish - example', function (test) {
  test.equal(packageName, "ddeal-publish");
});
