this.Ads = new Mongo.Collection("ads");

this.Ads.userCanInsert = function(userId, doc) {
	return true;
};

this.Ads.userCanUpdate = function(userId, doc) {
	return true;
};

this.Ads.userCanRemove = function(userId, doc) {
	return true;
};
