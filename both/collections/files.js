this.Files = new Mongo.Collection("files");

this.Files.userCanInsert = function(userId, doc) {
	return true;
};

this.Files.userCanUpdate = function(userId, doc) {
	return true;
};

this.Files.userCanRemove = function(userId, doc) {
	return true;
};
