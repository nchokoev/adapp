var pageSession = new ReactiveDict();

Template.NewAd.rendered = function() {
};

Template.NewAd.events({
	"click button.savenpublish":function(e,t){
		var pubList = [];
		if(Session.get("ddeal_publish") == true){
			pubList.push({fn:"ddeal_run", pre_check_fn:"ddeal_pre_check", password: $('input[name="ddeal_password"]').val()});
		}
		if(pubList.length > 0){
			Meteor.call("runSubmit", this.params._id, pubList, function(e,r){
				if(e){
					Bert.alert(e, 'danger');
				} else {
					Bert.alert("All Done.", 'success');
				}
			});
		}
    }
});

Template.NewAd.helpers({
	
});

Template.PluginShowTemplate.rendered = function() {
};

Template.PluginShowTemplate.events({
	
});

Template.PluginShowTemplate.helpers({
	// "id":function(){
	// 	return "Hello";//this.params._id;
	// }
});

Template.NewAdNewAdForm.rendered = function() {
	pageSession.set("newAdNewAdFormInfoMessage", "");
	pageSession.set("newAdNewAdFormErrorMessage", "");

	//console.log(this.data.params._id);
	var doc = Ads.findOne({_id:this.data.params._id});
	//console.log(doc);
	if(doc){
		$('input[name="_id"]').val(doc._id);
		$('input[name="title"]').val(doc.title);
		$('textarea[name="description"]').val(doc.description);
	}

	$(".input-group.date").each(function() {
		var format = $(this).find("input[type='text']").attr("data-format");

		if(format) {
			format = format.toLowerCase();
		}
		else {
			format = "mm/dd/yyyy";
		}

		$(this).datepicker({
			autoclose: true,
			todayHighlight: true,
			todayBtn: true,
			forceParse: false,
			keyboardNavigation: false,
			format: format
		});
	});

    //$("input[type='file']").fileinput();
	$("select[data-role='tagsinput']").tagsinput();
	$(".bootstrap-tagsinput").addClass("form-control");
	$("input[autofocus]").focus();
};

Template.NewAdNewAdForm.events({
	"click button.save": function(e, t){
		console.log("here");
		var _id = this.params._id;
		if(_id){
			Ads.update({_id:_id},{$set:{
				title: $('input[name="title"]').val(),
				description: $('textarea[name="description"]').val()
			}});
		} else {
			_id = Ads.insert({
				title: $('input[name="title"]').val(),
				description: $('textarea[name="description"]').val()
			});
			Router.go("/new_ad/"+_id);
		}
	},
	"submit": function(e, t) {
		e.preventDefault();
		var newAdNewAdFormMode = "insert";
		pageSession.set("newAdNewAdFormInfoMessage", "");
		pageSession.set("newAdNewAdFormErrorMessage", "");

		var self = this;

		function submitAction(msg) {
			if(!t.find("#form-cancel-button")) {
				switch(newAdNewAdFormMode) {
					case "insert": {
						$(e.target)[0].reset();
					}; break;

					case "update": {
						var message = msg || "Saved.";
						pageSession.set("newAdNewAdFormInfoMessage", message);
					}; break;
				}
			}

			/*SUBMIT_REDIRECT*/
		}

		function errorAction(msg) {
			msg = msg || "";
			var message = msg.message || msg || "Error.";
			pageSession.set("newAdNewAdFormErrorMessage", message);
		}

		validateForm(
			$(e.target),
			function(fieldName, fieldValue) {

			},
			function(msg) {

			},
			function(values) {
				
				switch(newAdNewAdFormMode) {
					case "insert": {
						newId = Ads.insert(values, function(e) { if(e) errorAction(e); else submitAction(); });
					}; break;

					case "update": {
						newId = Ads.update({"ownerId": Meteor.userId()}, values, function(e) { if(e) errorAction(e); else submitAction(); });
					}; break;
				}

			}
		);

		return false;
	},
	"click #form-cancel-button": function(e, t) {
		e.preventDefault();

		/*CANCEL_REDIRECT*/
	},
	"click #form-close-button": function(e, t) {
		e.preventDefault();

		/*CLOSE_REDIRECT*/
	},
	"click #form-back-button": function(e, t) {
		e.preventDefault();

		/*BACK_REDIRECT*/
	}	
});

Template.NewAdNewAdForm.helpers({
	"infoMessage": function() {
		return pageSession.get("newAdNewAdFormInfoMessage");
	},
	"errorMessage": function() {
		return pageSession.get("newAdNewAdFormErrorMessage");
	}
});

// Template.photosTmpl.rendered = function() {
// };

// Template.photosTmpl.events({
	
// });

// Template.photosTmpl.helpers({
// 	"files": function(){
// 		return Files.find({"ad_id": this.params._id});
// 	}
// });