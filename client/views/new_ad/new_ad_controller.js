this.NewAdController = RouteController.extend({
	template: "NewAd",

	yieldTemplates: {
		/*YIELD_TEMPLATES*/
	},

	onBeforeAction: function() {
		this.next();
	},

	action: function() {
		if(this.isReady()) { this.render(); } else { this.render("loading"); }
		/*ACTION_FUNCTION*/
	},

	isReady: function() {
		var subs = [
			Meteor.subscribe("ads_null"),
			Meteor.subscribe("files_list")
		];
		var ready = true;
		_.each(subs, function(sub) {
			if(!sub.ready())
				ready = false;
		});
		return ready;
	},

	data: function() {
		var data = {
			params: this.params || {},
			ads_null: Ads.findOne({}, {})
		};
		return data;
	},

	onAfterAction: function() {
		
	}
});