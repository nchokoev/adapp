var pageSession = new ReactiveDict();

Template.HomePrivate.rendered = function() {
	
};

Template.HomePrivate.events({
	
});

Template.HomePrivate.helpers({
	
});

var HomePrivateAdsListItems = function(cursor) {
	if(!cursor) {
		return [];
	}

	var searchString = pageSession.get("HomePrivateAdsListSearchString");
	var sortBy = pageSession.get("HomePrivateAdsListSortBy");
	var sortAscending = pageSession.get("HomePrivateAdsListSortAscending");
	if(typeof(sortAscending) == "undefined") sortAscending = true;

	var raw = cursor.fetch();

	// filter
	var filtered = [];
	if(!searchString || searchString == "") {
		filtered = raw;
	} else {
		searchString = searchString.replace(".", "\\.");
		var regEx = new RegExp(searchString, "i");
		var searchFields = ["title", "subtitle", "description", "photos", "mainPhoto"];
		filtered = _.filter(raw, function(item) {
			var match = false;
			_.each(searchFields, function(field) {
				var value = (getPropertyValue(field, item) || "") + "";

				match = match || (value && value.match(regEx));
				if(match) {
					return false;
				}
			})
			return match;
		});
	}

	// sort
	if(sortBy) {
		filtered = _.sortBy(filtered, sortBy);

		// descending?
		if(!sortAscending) {
			filtered = filtered.reverse();
		}
	}

	return filtered;
};

var HomePrivateAdsListExport = function(cursor, fileType) {
	var data = HomePrivateAdsListItems(cursor);
	var exportFields = [];

	var str = convertArrayOfObjects(data, exportFields, fileType);

	var filename = "export." + fileType;

	downloadLocalResource(str, filename, "application/octet-stream");
}


Template.HomePrivateAdsList.rendered = function() {
	pageSession.set("HomePrivateAdsListStyle", "table");
	
};

Template.HomePrivateAdsList.events({
	"submit #dataview-controls": function(e, t) {
		return false;
	},

	"click #dataview-search-button": function(e, t) {
		e.preventDefault();
		var form = $(e.currentTarget).parent();
		if(form) {
			var searchInput = form.find("#dataview-search-input");
			if(searchInput) {
				searchInput.focus();
				var searchString = searchInput.val();
				pageSession.set("HomePrivateAdsListSearchString", searchString);
			}

		}
		return false;
	},

	"keydown #dataview-search-input": function(e, t) {
		if(e.which === 13)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					var searchString = searchInput.val();
					pageSession.set("HomePrivateAdsListSearchString", searchString);
				}

			}
			return false;
		}

		if(e.which === 27)
		{
			e.preventDefault();
			var form = $(e.currentTarget).parent();
			if(form) {
				var searchInput = form.find("#dataview-search-input");
				if(searchInput) {
					searchInput.val("");
					pageSession.set("HomePrivateAdsListSearchString", "");
				}

			}
			return false;
		}

		return true;
	},

	"click #dataview-insert-button": function(e, t) {
		e.preventDefault();
		/**/
	},

	"click #dataview-export-default": function(e, t) {
		e.preventDefault();
		HomePrivateAdsListExport(this.ads_list, "csv");
	},

	"click #dataview-export-csv": function(e, t) {
		e.preventDefault();
		HomePrivateAdsListExport(this.ads_list, "csv");
	},

	"click #dataview-export-tsv": function(e, t) {
		e.preventDefault();
		HomePrivateAdsListExport(this.ads_list, "tsv");
	},

	"click #dataview-export-json": function(e, t) {
		e.preventDefault();
		HomePrivateAdsListExport(this.ads_list, "json");
	}

	
});

Template.HomePrivateAdsList.helpers({

	"insertButtonClass": function() {
		return Ads.userCanInsert(Meteor.userId(), {}) ? "" : "hidden";
	},

	"isEmpty": function() {
		return !this.ads_list || this.ads_list.count() == 0;
	},
	"isNotEmpty": function() {
		return this.ads_list && this.ads_list.count() > 0;
	},
	"isNotFound": function() {
		return this.ads_list && pageSession.get("HomePrivateAdsListSearchString") && HomePrivateAdsListItems(this.ads_list).length == 0;
	},
	"searchString": function() {
		return pageSession.get("HomePrivateAdsListSearchString");
	},
	"viewAsTable": function() {
		return pageSession.get("HomePrivateAdsListStyle") == "table";
	},
	"viewAsList": function() {
		return pageSession.get("HomePrivateAdsListStyle") == "list";
	},
	"viewAsGallery": function() {
		return pageSession.get("HomePrivateAdsListStyle") == "gallery";
	}

	
});


Template.HomePrivateAdsListTable.rendered = function() {
	
};

Template.HomePrivateAdsListTable.events({
	"click .th-sortable": function(e, t) {
		e.preventDefault();
		var oldSortBy = pageSession.get("HomePrivateAdsListSortBy");
		var newSortBy = $(e.target).attr("data-sort");

		pageSession.set("HomePrivateAdsListSortBy", newSortBy);
		if(oldSortBy == newSortBy) {
			var sortAscending = pageSession.get("HomePrivateAdsListSortAscending") || false;
			pageSession.set("HomePrivateAdsListSortAscending", !sortAscending);
		} else {
			pageSession.set("HomePrivateAdsListSortAscending", true);
		}
	}
});

Template.HomePrivateAdsListTable.helpers({
	"tableItems": function() {
		return this.ads_list.map(function(d){
			return{
				"id":d._id,
				"title":d.title,
				"subtitle":d.subtitle,
				"description":d.description,
				"photos":Files.find({"ad_id":d._id}).count(),
				"mainPhoto":Files.find({"ad_id":d._id}).count(),
			};
		});
	}
});


Template.HomePrivateAdsListTableItems.rendered = function() {
	
};

Template.HomePrivateAdsListTableItems.events({
	// "click td": function(e, t) {
	// 	e.preventDefault();
	// 	/**/
	// 	return false;
	// },

	"click .inline-checkbox": function(e, t) {
		e.preventDefault();

		if(!this || !this._id) return false;

		var fieldName = $(e.currentTarget).attr("data-field");
		if(!fieldName) return false;

		var values = {};
		values[fieldName] = !this[fieldName];

		Ads.update({ _id: this._id }, { $set: values });

		return false;
	},

	"click .delete-button": function(e, t) {
		e.preventDefault();
		var me = this;
		bootbox.dialog({
			message: "Delete? Are you sure?",
			title: "Delete",
			animate: false,
			buttons: {
				success: {
					label: "Yes",
					className: "btn-success",
					callback: function() {
						var f = Files.find({ad_id:t.data.id}).fetch();
						for(var i=0; i < f.length; i++){
							console.log(f[i]);
							S3.delete(f[i].relative_url, (function(_this) {
								return function(error, res) {
									if (!error) {
										var doc = Files.findOne({relative_url:_this.relative_url});
										if(doc){
											Files.remove({_id:doc._id});
											console.log("Removed: " + doc._id);
										}
										S3.collection.remove(_this._id);
										console.log("Removed: " + _this._id);
									} else {
										console.log(error);
									}
								};
							})(this));
						}
						console.log(t.data.id);
						Ads.remove({ _id: t.data.id });
					}
				},
				danger: {
					label: "No",
					className: "btn-default"
				}
			}
		});
		return false;
	},
	"click .edit-button": function(e, t) {
		e.preventDefault();
		Router.go("/new_ad/"+t.data.id);
		/**/
		return false;
	}
});

Template.HomePrivateAdsListTableItems.helpers({
	"checked": function(value) { return value ? "checked" : "" }, 
	"editButtonClass": function() {
		return Ads.userCanUpdate(Meteor.userId(), this) ? "" : "hidden";
	},

	"deleteButtonClass": function() {
		return Ads.userCanRemove(Meteor.userId(), this) ? "" : "hidden";
	}
});
