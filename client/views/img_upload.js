
Template.s3_tester.events({
    "change input.file_bag": function(){
        var _id = this.params._id;
        
        var files = $("input.file_bag")[0].files
        S3.upload({
                files:files,
                path:Meteor.userId()
          },function(e,r){
            if(e) return;
            if(_id){
              r.ad_id = _id;
              console.log(r);
              Files.insert(r);
            } else {
                _id = Ads.insert({
                    title: $('input[name="title"]').val(),
                    description: $('textarea[name="description"]').val()
                });
                r.ad_id = _id;
                console.log(r);
                Files.insert(r);
                Router.go("/new_ad/"+_id);
            }
        });
    },
    "click button.delete": function(event){
        S3.delete(this.relative_url, (function(_this) {
        return function(error, res) {
          if (!error) {
            var doc = Files.findOne({relative_url:_this.relative_url});
            if(doc){
              Files.remove({_id:doc._id});
            }
            return S3.collection.remove(_this._id);
          } else {
            return console.log(error);
          }
        };
      })(this));  
    },
    "click button.rotate-right": function(e, t){
        var rot = 0;
        var im = $("div.show-image img#"+this._id);
        if(im.hasClass("img90") == true){
            im.removeClass("img90");
            im.addClass("img180");
            rot = 180;
        } else if(im.hasClass("img180") == true){
            im.removeClass("img180");
            im.addClass("img270");
            rot = 270;
        } else if(im.hasClass("img270") == true){
            im.removeClass("img270");
            rot = 0;
        } else {
            im.addClass("img90");
            rot = 90;
        }
        Files.update({_id:this._id},{$set:{"rot":rot}});
    },
    "click button.rotate-left": function(e, t){
        var rot = 0;
        var im = $("div.show-image img#"+this._id);
        if(im.hasClass("img90") == true){
            im.removeClass("img90");
            rot = 0;
        } else if(im.hasClass("img180") == true){
            im.removeClass("img180");
            im.addClass("img90");
            rot = 90;
        } else if(im.hasClass("img270") == true){
            im.removeClass("img270");
            im.addClass("img180");
            rot = 180;
        } else {
            im.addClass("img270");
            rot = 270;
        }
        Files.update({_id:this._id},{$set:{"rot":rot}});
    }
})

Template.s3_tester.helpers({
    "files": function(){
        return Files.find({"ad_id": this.params._id});
    }
})

Template.imgUploadForm.onCreated(function () {
  this.currentUpload = new ReactiveVar(false);
});

Template.imgUploadForm.rendered=function(){
    //$("input[type='file']").fileinput({'showUpload':false, 'previewFileType':'any'});
};

Template.uploadedFiles.helpers({
  uploadedFiles: function () {
    return Images.find();
  }
});

Template.imgUploadForm.helpers({
  currentUpload: function () {
      console.log("imgUploadForm.helpers");
    return Template.instance().currentUpload.get();
  }
});

Template.imgUploadForm.events({
  'change #imgFileInput': function (e, template) {
    console.log("Upload Start.");
    if (e.currentTarget.files && e.currentTarget.files[0]) {
      // We upload only one file, in case
      // multiple files were selected
      var upload = Images.insert({
        file: e.currentTarget.files[0],
        streams: 'dynamic',
        chunkSize: 'dynamic'
      }, false);

      upload.on('start', function () {
        template.currentUpload.set(this);
      });

      upload.on('end', function (error, fileObj) {
        if (error) {
          alert('Error during upload: ' + error);
        } else {
          alert('File "' + fileObj.name + '" successfully uploaded');
        }
        template.currentUpload.set(false);
      });
      upload.start();
    }
  }
});