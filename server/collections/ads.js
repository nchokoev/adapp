Ads.allow({
	insert: function (userId, doc) {
		return Ads.userCanInsert(userId, doc);
	},

	update: function (userId, doc, fields, modifier) {
		return Ads.userCanUpdate(userId, doc);
	},

	remove: function (userId, doc) {
		return Ads.userCanRemove(userId, doc);
	}
});

Ads.before.insert(function(userId, doc) {
	doc.createdAt = new Date();
	doc.createdBy = userId;
	doc.modifiedAt = doc.createdAt;
	doc.modifiedBy = doc.createdBy;

	
	if(!doc.ownerId) doc.ownerId = userId;
});

Ads.before.update(function(userId, doc, fieldNames, modifier, options) {
	modifier.$set = modifier.$set || {};
	modifier.$set.modifiedAt = new Date();
	modifier.$set.modifiedBy = userId;

	
});

Ads.before.upsert(function(userId, selector, modifier, options) {
	modifier.$set = modifier.$set || {};
	modifier.$set.modifiedAt = new Date();
	modifier.$set.modifiedBy = userId;

	/*BEFORE_UPSERT_CODE*/
});

Ads.before.remove(function(userId, doc) {
	
});

Ads.after.insert(function(userId, doc) {
	
});

Ads.after.update(function(userId, doc, fieldNames, modifier, options) {
	
});

Ads.after.remove(function(userId, doc) {
	
});
